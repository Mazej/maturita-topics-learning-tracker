import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FirebaseServiceService } from '../service/firebase-service.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public email = new FormControl('', [Validators.required, Validators.email]);
  public password = new FormControl('', [Validators.required]);
  public loginError = null;
  @Output('loggedIn') loggedIn: EventEmitter<void> = new EventEmitter();
  constructor(public firebaseService: FirebaseServiceService) {}

  ngOnInit(): void {
    let email = localStorage.getItem('email');
    let passwd = localStorage.getItem('password');

    if (email !== null && passwd !== null) {
      this.onSignIn(email, passwd);
    }
  }
  async onSignIn(email: string, password: string) {
    // this.loginValid = false;
    try {
      await this.firebaseService.signin(email, password);
      if (this.firebaseService.isLoggedIn) {
        this.loggedIn.emit();
      }
    } catch (e) {
      console.log('Login failed');
      this.loginError = e.message;
    }
  }
}
