import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AngularFirestore, DocumentData } from '@angular/fire/firestore';
import { ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
// Interfaces
import { Subject } from './../interfaces/subject';
import { Question } from './../interfaces/question';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class MainNavComponent implements OnInit {
  selectedSubject: Subject;
  selectedSubjectView: String = '';
  question: Question;

  subjects = this.fireStore.collection('subjects').valueChanges();
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  questionsRef: Observable<DocumentData[]>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private fireStore: AngularFirestore
  ) {}

  ngOnInit(): void {
    this.loadSubjectAndQuestion();
  }

  toggleSidenav(nav: MatSidenav): void {
    const isSmallScreen = this.breakpointObserver.isMatched(
      '(max-width: 599px)'
    );
    if (isSmallScreen) {
      nav.toggle();
    }
  }

  setSubject(subject: Subject): void {
    this.selectedSubject = subject;
    this.selectedSubjectView = subject.name;
    this.questionsRef = this.fireStore
      .collection('subjects')
      .doc(subject.shortName)
      .collection('questions')
      .valueChanges();
    sessionStorage.setItem('subject', JSON.stringify(this.selectedSubject));
  }

  setQuestion(value: Question): void {
    this.question = value;
    sessionStorage.setItem('question', JSON.stringify(value));
  }

  saveChanges(question: Question): void {
    this.fireStore
      .collection('subjects')
      .doc(this.selectedSubject.shortName)
      .collection('questions')
      .doc(String(question.number))
      .set(question);
    sessionStorage.setItem('question', JSON.stringify(question));
  }

  loadSubjectAndQuestion(): void {
    this.question = JSON.parse(sessionStorage.getItem('question'));
    let subject = JSON.parse(sessionStorage.getItem('subject'));
    if (subject != null) this.setSubject(subject);
  }
}
