import { Component } from '@angular/core';
import { Subject as RXJSSubj } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'pitr-mat-otazky';
  isSignedIn: RXJSSubj<boolean> = new RXJSSubj();

  loggedIn(): void {
    this.isSignedIn.next(true);
  }
}
