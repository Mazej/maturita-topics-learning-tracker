import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class FirebaseServiceService {
  isLoggedIn = false;
  constructor(public firebaseAuth: AngularFireAuth) {}

  async signin(email: string, password: string) {
    this.firebaseAuth.setPersistence('local');
    await this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then((_) => {
        this.isLoggedIn = true;
        localStorage.setItem('password', password);
        localStorage.setItem('email', email);
      });
  }
  logout() {
    this.firebaseAuth.signOut();
    localStorage.removeItem('email');
    localStorage.removeItem('passwd');
  }
}
