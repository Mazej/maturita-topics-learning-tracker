import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Subject as RXJSSubj } from 'rxjs';
import { Question } from './../interfaces/question';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.css'],
})
export class QuestionDetailsComponent implements OnChanges {
  @Input() question: Question;
  @Output() saveEvent = new EventEmitter<Question>();

  unsaved: RXJSSubj<boolean> = new RXJSSubj();

  ngOnChanges(changes: SimpleChanges): void {
    this.unsaved.next(false);
  }

  setUnsaved(): void {
    this.unsaved.next(true);
  }

  saveChanges(): void {
    this.saveEvent.emit(this.question);
    this.unsaved.next(false);
  }
}
