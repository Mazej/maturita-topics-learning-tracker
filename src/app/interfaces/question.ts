export interface Question {
  number: number;
  topic: string;
  description: string;
  note: string;
  completion: number;
}
