export interface Subject {
  name: string;
  questions: any;
  shortName: string;
}
