<div id="top"></div>
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br /> <div align="center">
<a href="https://gitlab.com/Mazej/maturita-topics-learning-tracker">
<img src="images/logo.png" alt="Logo" width="157" height="157">
</a>

  <h3 align="center">Maturita topics learning tracker </h3>

  <p align="center">
Simple web application for writing notes and tracking the learning progress of topics related to an exam.
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="mat-topics.kubikm.xyz">View Demo</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#list-of-features">List of features</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#custom-hosting">Custom hosting</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

My friend was finishing a highschool which is in our country accomplished by passing a final exam. The exam consists of multiple topics related to subjects taught during the previous years in a high school. So I created a simple web application where she could write some notes and track her learning progress on the exam topics. The application currently does not allow other users to register, since it was a project written for her only.

<p align="right">(<a href="#top">back to top</a>)</p>

### List of features
- Simple UI
- Ability to save a note about each topic
- Ability to track the progress of learning each topic

### Built With

- [![Angular][angular.io]][angular-url]
- [![Firebase][firebase.google.com]][firebase-url]
- [![NodeJs][nodejs.org]][nodejs-url]

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

1. Login - simply fill in your email and password.
   <img src="images/Login.jpg" alt="Login">
2. Choose a subject
   <img src="images/Choose-a-subject.jpg" alt="Choose a subject">
3. Choose a topic
   <img src="images/Choose-a-topic.jpg" alt="Choose a topic">
4. Edit note and progress bar, save changes.
   <img src="images/Edit-note-and-progress-bar-save.jpg" alt="Edit note and progress bar, save">

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->
<!--
## Roadmap

- [x] Add Changelog
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
  - [ ] Chinese
  - [ ] Spanish

See the [open issues](https://gitlab.com/Mazej/maturita-topics-learning-tracker/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p> -->

<!-- CONTACT -->

## Contact

Matěj Kubík - matelf.kubik@gmail.com

Project Link: [https://gitlab.com/Mazej/maturita-topics-learning-tracker](https://gitlab.com/Mazej/maturita-topics-learning-tracker)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.jpg
[angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[angular-url]: https://angular.io/
[nodejs.org]: https://img.shields.io/badge/Node.js-93c541?style=for-the-badge&logo=node.js&logoColor=gray
[nodejs-url]: https://nodejs.org
[firebase-url]: https://firebase.google.com
[firebase.google.com]: https://img.shields.io/badge/Firebase-ffcc30?style=for-the-badge&logo=firebase&logoColor=black

## Custom hosting

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
